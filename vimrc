
"" PLUGINS
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'lifepillar/vim-context-metapost'
Plugin 'VundleVim/Vundle.vim'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'tomtom/tcomment_vim'
Plugin 'sirver/ultisnips'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'ryanoasis/vim-devicons'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-surround'
Plugin 'valloric/youcompleteme'
Plugin '2072/PHP-Indenting-for-VIm'
Plugin 'jiangmiao/auto-pairs'
Plugin 'kabbamine/zeavim.vim'
Plugin 'evidens/vim-twig'
Plugin 'majutsushi/tagbar'
Plugin 'lilydjwg/colorizer'
Plugin 'dpelle/vim-Grammalecte'
Plugin 'christoomey/vim-system-copy'
Plugin 'honza/vim-snippets'
Plugin 'digitaltoad/vim-pug'
" Python IDE  
Plugin 'nvie/vim-flake8'
" Plugin 'python-mode/python-mode'


call vundle#end()            " required
filetype plugin indent on    " requiredset backspace=indent,eol,start

" # GENERAL
set backspace=indent,eol,start
syntax on
set mouse=a
set encoding=UTF-8
set shell=/usr/bin/fish

" -- AUTOCOMPLETION -- "
filetype plugin on
set omnifunc=syntaxcomplete#Complete

" number lines
" set nu!
set number
set rnu

set numberwidth=4
set cpoptions+=n

set path+=**
set wildmenu
vnoremap <C-C> :w !xclip -i -sel c<CR><CR>

" THEME
set background=light
colors koehler  

" MMMAAAPPP
let mapleader = ","
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprevious<CR>
nnoremap <C-r> :redo<CR>
map <leader>nt :NERDTreeToggle<cr>
map <leader>n :tabNext<cr>
map <leader>t :tabe $HOME/.tips/content<cr>

" TABULATION
set shiftwidth=2
set tabstop=2

" config plugins
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'
let g:airline_theme = 'raven'

let g:grammalecte_cli_py='/home/antoine/Programmes/grammalecte-cli.py'

nnoremap <F5> :Tagbar<cr>
