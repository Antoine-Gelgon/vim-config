# Vim-config
Antoine Gelgon 2019

## For VIM
Juste you need to run this :

1. `git clone git@gitlab.com:Antoine-Gelgon/vim-config.git ~/.vim/`
2. `ln -s $HOME/.vim/vimrc ~/.vimrc`
3. `git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
4. Open vim and enter `:PluginInstall`

## For NEOVIM

1. `git clone git@gitlab.com:Antoine-Gelgon/vim-config.git ~/.vim/`
2. `ln -s $HOME/.vim/vimrc ~/.config/nvim/init.vim`
3. `ln -s $HOME/.vim/colors/ ~/.config/nvim/colors`
4. `git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`
5. Open neovim and enter `:PluginInstall`

### Clipboard

In linux if clipboard don't works, you must install `xclip`.

## Grammalecte Install

You need to download grammalecte programme (server&cli) for use the plugin. -> [](https://grammalecte.net/#download)

## Ressources

[Vim Cheat Sheet](https://vim.rtorr.com/)
